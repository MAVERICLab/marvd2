#!/usr/bin/env python

import argparse
import gzip
import logging
import os
import pickle
import shutil
import subprocess
import sys
import time
from importlib.metadata import version
from pathlib import Path
import swifter
import joblib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import psutil
from Bio import SeqIO
from ete3 import NCBITaxa
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import RFECV
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split

# Attempt to reduce memory overhead when installing via (bio)conda
disable_vaex = False
try:
    import vaex
except ImportError:  # ModuleNotFoundError
    disable_vaex = True

os.environ["MODIN_ENGINE"] = "dask"  # Modin will use Dask

logging.basicConfig(level=logging.INFO,
                    format='[%(levelname)s] %(asctime)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S %p')

# Create logger
logger = logging.getLogger(f'MArVD2 {version("MArVD2")}')

pd.set_option('display.max_rows', 100)
pd.set_option('display.max_columns', 10)
pd.set_option('display.width', 1000)

parser = argparse.ArgumentParser(description="MArvD2 is a tool to classify the enigmatic archaeal viruses.")

options = parser.add_argument_group('Input and Outputs')

options.add_argument('-i', '--input-fp', dest='input_fp', metavar='FILEPATH',
                     help="Input sequences/contigs/genomes to predict.")

options.add_argument('-m', '--load-models', dest='model_fp', metavar='FILEPATH',
                     help="Trained model to use.")

options.add_argument('--build-models', dest='build_models', action='store_true',
                     help="Enable to generate models based on input data. MUST include archaeal terms.")

options.add_argument('-o', '--output-dir', dest='output_dir', metavar='DIRPATH', default='MArVD2-OutDir',
                     help="Output directory where all results will be stored.")

options.add_argument('-a', '--archaeal-terms', dest='archaeals', action='append',
                     help="Terms to use for classifying sequence as an archaeal virus. Use this repeatedly to denote "
                          "multiple terms, e.g. -a ETNP -a archaea -a portal_prot5")

options.add_argument('--dbs-dir', dest='dbs_dir', metavar='DIRPATH',
                     help="Directory with ALL database files. MArVD2 will assume that all DB files are in this "
                          "directory unless specified individually.")

data_opts = parser.add_argument_group('Databases and files')

data_opts.add_argument('--db-pvog', dest='pvog_db', metavar='FILEPATH',
                       help="Location of pVOG HMM profiles. MUST have the *.hmm extension. Can be downloaded from: "
                            "https://ftp.ncbi.nlm.nih.gov/pub/kristensen/pVOGs/downloads.html")

data_opts.add_argument('--db-nr', dest='nr_db', metavar='FILEPATH',
                       help="Location of the NR database. MUST be in AMINO ACID format and have the *.faa extension.")

data_opts.add_argument('--db-accession2tax', dest='accession2tax', metavar='FILEPATH',
                       help="Location of the prot.accession2taxid data file, downloaded from the NCBI taxonomy "
                            "database and subsequently parsed to remove several columns to minimize disk space.")

data_opts.add_argument('--marine-jackhmmer-db', dest='marine_jackhmmer_db', metavar='FILEPATH',
                       help="Location of the pVOG+environmental archaea database. MUST be in AMINO ACID format and "
                            "have the *.faa extension.")

data_opts.add_argument('--other-jackhmmer-db', dest='other_jackhmmer_db', metavar='FILEPATH',
                       help="(Optional) Location of the pVOG+other archaea database. MUST be in AMINO ACID format and "
                            "have the *.faa extension.")

data_opts.add_argument('--viral-refseq-txt', dest='refseq_viruses', metavar='FILEPATH',
                       help="Location of the viruses.txt file of viral refseq. Can be downloaded from "
                            "ftp.ncbi.nlm.nih.gov/genomes/GENOME_REPORTS/viruses.txt")

data_opts.add_argument('--pvog-dir', dest='pvog_dir', metavar='DIRPATH',
                       help="Location of the pVOG data DIRECTORY. Needs to contain the VOGTable.txt and "
                            "ViralQuotient.txt files. Can be downloaded from "
                            "https://ftp.ncbi.nlm.nih.gov/pub/kristensen/pVOGs/")

data_opts.add_argument('--no-dbs', dest='no_dbs', action='store_true',
                       help="Skip processing related to database searches. This will assume output results files from "
                            "the searches are present in the output directory")

misc_opts = parser.add_argument_group('Misc Options')

misc_opts.add_argument('--num-features', dest='num_features', type=int, default=5,
                       help="Number of features to use in either a straight-up classification, or maximum value to use "
                            "for recursive feature elimination.")

misc_opts.add_argument('--enable-gpu', dest='enable_gpu', action='store_true',
                       help="Enable GPU acceleration (IF POSSIBLE). If GPUs are not available, then this option will "
                            "not be activated.")

misc_opts.add_argument('-c', '--cpu-count', dest='cpu_count', type=int, default=psutil.cpu_count(logical=False),
                       help="Number of CPUs to use during processing. Not all parts of MArVD2 are multi-threaded.")

misc_opts.add_argument('--reload-taxonomy', dest='reload_tax', action='store_true',
                       help="Update ETE3's database and exit.")

misc_opts.add_argument('--disable-vaex', dest='disable_vaex_opt', action='store_true',
                       help="Disable the use of Vaex. This will not generate a HDF file and instead use Pandas to "
                            "directly read the input file(s).")

args = parser.parse_args()


def error(msg):
    sys.stderr.write(f'ERROR: {msg}')
    sys.exit(1)


enable_gpu = False
if args.enable_gpu:
    logger.info('Attempting to enable GPU...')

    try:
        import cudf
        enable_gpu = True

        logger.info('GPU enabled! Running (portions) of MArVD2 as GPU-enhanced!')
    except ImportError as e:
        logger.info('Unable to load cuDF. GPU-optimized code NOT enabled.')
        enable_gpu = False

ncbi = NCBITaxa()
if args.reload_tax:
    logger.info('Updating ETE3 taxonomy database to latest version and exiting.')
    ncbi.update_taxonomy_database()
    exit(0)

if args.disable_vaex_opt:
    disable_vaex = True


def execute(command: str, run_silent=False):
    logger.info(f'Running command: {command}')
    if run_silent:
        subprocess.check_call(command, shell=True, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
    else:
        subprocess.check_call(command, shell=True)


class FeatureTable(object):
    """
    Handle features
    """

    def __init__(self, tracker: dict, add_refs=False):
        """
        Init the object with basic details and build from that point. Can't start until proteins are generated
        """

        # Where to find files
        self.output_dir = tracker['Output Directory']
        self.nucleotide_fp = tracker['Nucleotide']
        self.prodigal_genes_fp = tracker['Prodigal Genes']
        self.prodigal_aa_fp = tracker['Prodigal AA']

        # For model building
        self.refseq = None  # Will become df
        self.add_refs = add_refs  # Added once refs are added

        # Where to save class progress
        self.basic_fp = tracker['Features']
        self.advanced_fp = tracker['Features-Advanced']

        # Each of the database(s) and/or runs
        self.db = tracker['Databases']  # "Output" DB, not source
        self.pvog_db = tracker['Database-pVOG_HMMs']
        self.nr_db = tracker['Database-NR']
        self.marine_db = tracker['Database-Marine']
        try:
            self.other_jackhmmer_db = Path(args.other_jackhmmer_db)
        except TypeError:
            self.other_jackhmmer_db = None
        self.hmmscan = tracker['HMMScan']
        self.mmseqs = tracker['MMSeqs2']
        self.marine_jackhmmer = tracker['Marine Jackhmmer']
        self.other_jackhmmer = tracker['Other Jackhmmer']
        self.acc2taxid_fp = tracker['Database-Accession2TaxID']
        self.pvog_dir = tracker['Database-pVOGs']

        # Build initial table with basic statistics
        if not hasattr(self, 'feature_tbl'):
            self.feature_tbl = self.build_table()

        # Save after initial table
        if not self.basic_fp.exists():
            with open(self.basic_fp, 'wb') as basic_fh:
                pickle.dump(self, basic_fh)

        if self.add_refs:
            # Accessing from "global" variable?
            logger.info('Adding references to feature table')
            self.add_references(args.archaeals, args.refseq_viruses)
            self.contains_refs = True

            # Database searches - Always check for already done analyses appended to dataframe
            logger.debug('Feature table after adding references:')
            logger.debug(self.feature_tbl)

        if not hasattr(self, 'run_hmmscan'):

            # Check for HMMscan vs pVOGs
            if not self.hmmscan.exists():  # Will need to run it

                # Set up HMM DB
                hmm_db = self.check_and_build_hmm_db()

                # Run HMMsearch vs pVOGs
                self.run_hmmsearch(hmm_db)

            # Get pVOG info
            pVOG_dir = self.pvog_dir
            pVOG_fp = pVOG_dir / 'VOGTable.txt'
            vq_fp = pVOG_dir / 'ViralQuotient.txt'

            vq_df = pd.read_csv(vq_fp, header=0, index_col=0, delimiter='\t')
            vq_df.rename(index={'#VOG': 'VOG'}, inplace=True)

            pVOG_headers = ['VOG', 'Host Domain', 'Proteins', 'Genomes', 'Virus Families', 'Virus Genera', 'Annotation',
                            'Mapping']
            pVOG_df = pd.read_csv(pVOG_fp, header=None, index_col=0, delimiter='\t', names=pVOG_headers)

            # HMMscan vs pVOG DB
            hmmscan_df = self.parse_hmmsearch()

            for genome, genome_df in hmmscan_df.groupby(by='genome'):
                # genes = self.feature_tbl.loc[genome, 'Genes'] --> started replacing .loc with .at for single values
                genes = self.feature_tbl.at[genome, 'Genes']
                # If same gene hits to different pVOGs, then will count as 2
                self.feature_tbl.loc[genome, 'pVOG Total Hits'] = len(genome_df)  # pvog_hits
                self.feature_tbl.loc[genome, 'pVOG Unique Hits'] = len(genome_df['target name'].unique())

                # Get subset of pVOG info (w/ host, protein, genomes, annotations, mapping, etc)
                pvogs_hit = pVOG_df.loc[genome_df['query name']]

                # Identify which domain the pVOG hits belong to
                # Previously used value_counts to get sums, but need to access more detail than provided by counts
                for host, host_df in pvogs_hit.groupby(by='Host Domain'):
                    host_unique_hits = len(host_df)
                    self.feature_tbl.loc[genome, f'pVOG {host}'] = host_unique_hits
                    self.feature_tbl.loc[genome, f'pVOG {host} % Genes Matched'] = host_unique_hits / genes

                    # Get average VQ for the pVOGs (are these pVOGs found in bacteria too?, if so, weight less heavily)
                    mean_vq = vq_df.loc[host_df.index, 'Viral Quotient'].mean()
                    self.feature_tbl.loc[genome, f'pVOG {host} VQ'] = mean_vq

            self.run_hmmscan = True

            with open(self.basic_fp, 'wb') as basic_fh:
                pickle.dump(self, basic_fh)

        if not hasattr(self, 'run_mmseqs'):

            # Check for HMMscan vs pVOGs
            if not self.mmseqs.exists():  # If csv doesn't exist

                logger.info('Building MMSeqs2 results...')

                # Set up MMSeqs2 query DB
                query_db = self.prodigal_aa_fp.parent / f'{self.prodigal_aa_fp.stem}_DB'
                query_db = self.check_and_build_mmseq_db(self.prodigal_aa_fp, query_db)

                # Set up MMSeqs2 target DB
                target_db = self.nr_db.parent / f'{self.nr_db.stem}_DB'
                target_db = self.check_and_build_mmseq_db(self.nr_db, target_db)

                # Run MMSeq2 vs NR
                mmseq_out_db = self.prodigal_aa_fp.parent / f'{self.prodigal_aa_fp.stem}.MMSeq2'
                tmp_db = self.prodigal_aa_fp.parent / f'{self.prodigal_aa_fp.stem}_TMP'
                mmseq_tab_fp = self.run_mmseq(query_db, target_db, mmseq_out_db, tmp_db)

                # HAVE to identify which proteins are archaeal or bacterial, at least give taxid to parse out later
                self.parse_mmseq(mmseq_tab_fp, self.acc2taxid_fp)

            elif not self.mmseqs.exists() and Path(str(self.mmseqs).replace('.csv', '.tab')).exists():
                # Execution stopped before finishing
                mmseq_tab_fp = Path(str(self.mmseqs).replace('.csv', '.tab'))
                self.parse_mmseq(mmseq_tab_fp, self.acc2taxid_fp)

            # MMSeq2 - vs NR
            mmseqs2_df = pd.read_csv(self.mmseqs, header=0, index_col=0)  # filter_mmseq
            mmseqs2_df['taxid'] = mmseqs2_df['taxid'].fillna(-1)
            mmseqs2_df['taxid'] = mmseqs2_df['taxid'].astype(int)
            mmseqs2_df['taxid'] = mmseqs2_df['taxid'].astype(str)  # Really don't like NaN

            logger.info('Parsing taxonomy IDs...')
            start_time = time.process_time()
            mmseqs2_df['host'] = mmseqs2_df.swifter.progress_bar(False).apply(lambda x: parse_taxid(x['taxid']), axis=1)
            logger.info(f'It took {time.process_time() - start_time} seconds to process taxonomy IDs')

            new_start = time.process_time()

            # NR can match to *anything*, so matches might not necessarily be good
            for genome, genome_df in mmseqs2_df.groupby(by='genome'):

                self.feature_tbl.loc[genome, 'MMSeqs2 Target Matches (Total)'] = len(genome_df)
                unique_hits = len(genome_df.drop_duplicates(subset='qaccver', keep='first'))
                self.feature_tbl.loc[genome, 'MMSeqs2 Target Matches (Unique)'] = unique_hits

                for target, target_df in genome_df.groupby(by='qaccver'):  # Don't like having to do TWO for loops

                    # Need to group by "host", as some hosts (i.e. viruses) are more useful than bacteria/archaea
                    for host, host_df in target_df.groupby(by='host'):

                        if host == 'viruses':  # Need to be specific here, does it exist in the refseq database?

                            if isinstance(self.refseq, pd.DataFrame):

                                try:
                                    taxids = [int(taxid) for taxid in host_df['taxid']]
                                    virus_host_s = self.refseq[
                                        self.refseq['TaxID'].isin(taxids)]['Host']

                                    virus_host = '/'.join(sorted(virus_host_s.unique().tolist()))
                                    host = f'{host} ({virus_host})'

                                except IndexError:
                                    host = f'{host} (unknown)'  # Really, host = viruses

                        sorted_target_df = target_df.sort_values(by='evalue',
                                                                 ascending=True)  # If want to take top N

                        avg_evalue = sorted_target_df['evalue'].mean()
                        avg_score = sorted_target_df['bitscore'].mean()

                        self.feature_tbl.loc[genome, f'MMSeqs2 Avg {host} E-Value'] = avg_evalue
                        self.feature_tbl.loc[genome, f'MMSeqs2 Avg {host} Score'] = avg_score

                        # % genes w/ no mmseq2 hit w/ marine archaeal viruses
                        # % genes w/ a mmseq2 hit w/ marine archaeal viruses
                        # mean score marine archaeal virus mmseq2

                        # % genes w/ no mmseq2 hit w/ nr
                        # % genes w/ mmseq2 hit w/ phage nr
                        # mean score phage nr mmseq2 hit
                        # % genes w/ mmseq2 hit w/ archaeal virus nr
                        # mean score archaeal virus NR mmseq2 hit

            logger.info(f'It took {time.process_time() - new_start} seconds to post process MMSeqs2 genomes')

            # Ensure there's no NaN values
            mmseqs2_evalue_cols = [col for col in self.feature_tbl.columns if 'MMSeqs2 Avg' and 'E-Value' in col]
            self.feature_tbl[mmseqs2_evalue_cols] = self.feature_tbl[mmseqs2_evalue_cols].fillna(1000)
            mmseqs2_score_cols = [col for col in self.feature_tbl.columns if 'MMSeqs2 Avg' and 'Score' in col]
            self.feature_tbl[mmseqs2_score_cols] = self.feature_tbl[mmseqs2_score_cols].fillna(0)

            self.run_mmseqs = True

            with open(self.basic_fp, 'wb') as basic_fh:
                pickle.dump(self, basic_fh)

        for jackhammer_run in ['Marine', 'Other']:

            if not hasattr(self, f'run_{jackhammer_run}_jackhmmer'):

                jackhmmer_run = self.other_jackhmmer if jackhammer_run == 'Other' else self.marine_jackhmmer

                if not jackhmmer_run.exists():  # Will need to run it

                    logger.info(f'Running {jackhammer_run} Jackhmmer...')

                    # Run Jackhmmer
                    if jackhammer_run == 'Marine':
                        target_faa = self.marine_db  # pVOGs?
                    else:  # Other
                        if self.other_jackhmmer_db:
                            target_faa = self.other_jackhmmer_db
                        else:
                            target_faa = None
                    # Only tool which does not require copying DBs, only needs
                    if target_faa:
                        self.runner_jackhmmer(target_faa, jackhammer_run.lower())
                        # read access to faa, one of which is created a few steps earlier in the output directory

                # Jackhmmer - vs arch pVOG + environ
                jackhmmer_df = self.parse_jackhmmer(jackhammer_run)

                if not jackhmmer_df.empty:

                    # Remove self-hits
                    # Double-check, but query != target because target has extra '_' in name???
                    jackhmmer_df['target name'] = jackhmmer_df['target name'].str.replace('__', '_')  # Now removes ~100
                    jackhmmer_df = jackhmmer_df[jackhmmer_df['target name'] != jackhmmer_df['query name']]

                    # Hits per CDS to deal with multiple hits? Best hit (i.e. 1 hit max) per CDS?
                    for genome, genome_df in jackhmmer_df.groupby(by='genome'):
                        genes = self.feature_tbl.loc[genome, 'Genes']

                        # Get unique hits per target - "how many times does the target appear uniquely in the matches?"
                        self.feature_tbl.loc[genome, f'{jackhammer_run} Jackhmmer Total Target Matches'] = len(genome_df)
                        unique_hits = len(genome_df.drop_duplicates(subset='target name', keep='first'))
                        self.feature_tbl.loc[genome, f'{jackhammer_run} Jackhmmer Unique Target Matches'] = unique_hits

                        self.feature_tbl.loc[genome, f'{jackhammer_run} Jackhmmer No Matches'] = unique_hits / genes

                        avg_evalues = []
                        avg_scores = []
                        for target, target_df in genome_df.groupby(
                                by='target name'):  # Don't like having to do TWO for loops
                            sorted_target_df = target_df.sort_values(by='E-value (full)', ascending=True)

                            avg_evalue = sorted_target_df['E-value (full)'].mean()
                            avg_score = sorted_target_df['score (full)'].mean()

                            avg_evalues.append(avg_evalue)
                            avg_scores.append(avg_score)

                        avg_e = np.average(avg_evalues)
                        avg_s = np.average(avg_scores)

                        self.feature_tbl.loc[genome, f'{jackhammer_run} Jackhmmer Avg E-Value'] = avg_e
                        self.feature_tbl.loc[genome, f'{jackhammer_run} Jackhmmer Avg Score'] = avg_s
                        self.feature_tbl.loc[genome, f'{jackhammer_run} Jackhmmer % Genes Matched'] = len(avg_scores) / genes

                    # Ensure there's no NaN values
                    jackhmmer_evalue_cols = [col for col in self.feature_tbl.columns if
                                             f'{jackhammer_run} Jackhmmer Avg' and 'E-Value' in col]
                    self.feature_tbl[jackhmmer_evalue_cols] = self.feature_tbl[jackhmmer_evalue_cols].fillna(1000)
                    jackhmmer_score_cols = [col for col in self.feature_tbl.columns if f'{jackhammer_run} Jackhmmer Avg'
                                            and 'Score' in col]
                    self.feature_tbl[jackhmmer_score_cols] = self.feature_tbl[jackhmmer_score_cols].fillna(0)

                    # TODO Handle run flag if DB doesn't exist, yet user wants to re-run. Might only add 1-2 sec, but
                    # the code doesn't need to have all that extra
                    if jackhammer_run == 'Marine':
                        self.run_Marine_jackhmmer = True
                    else:  # Other
                        self.run_other_jackhmmer = True

                with open(self.basic_fp, 'wb') as basic_fh:
                    pickle.dump(self, basic_fh)

        # Correct ordering of columns
        # If columns aren't the right num or ordering, it ruins the ML
        ref_columns = ['Host', 'Viral TaxID', 'Archaeal']
        column_order = ['Name', 'Genes', 'Avg Gene Len', 'Gene Density', 'Strand Shifts', 'pVOG Total Hits',
                        'pVOG Unique Hits', 'pVOG Bacteria', 'pVOG Bacteria % Genes Matched', 'pVOG Bacteria VQ',
                        'pVOG Bacteria+Archaea', 'pVOG Bacteria+Archaea % Genes Matched', 'pVOG Bacteria+Archaea VQ',
                        'pVOG Archaea', 'pVOG Archaea % Genes Matched', 'pVOG Archaea VQ',
                        'MMSeqs2 Target Matches (Total)', 'MMSeqs2 Target Matches (Unique)',
                        'MMSeqs2 Avg bacteria E-Value', 'MMSeqs2 Avg bacteria Score', 'MMSeqs2 Avg eukaryota E-Value',
                        'MMSeqs2 Avg eukaryota Score', 'MMSeqs2 Avg archaea E-Value', 'MMSeqs2 Avg archaea Score',
                        'MMSeqs2 Avg viruses E-Value', 'MMSeqs2 Avg viruses Score', 'MMSeqs2 Avg Unknown E-Value',
                        'MMSeqs2 Avg Unknown Score']

        jackhmmer_cols = ['Jackhmmer Total Target Matches',
                          'Jackhmmer Unique Target Matches', 'Jackhmmer No Matches', 'Jackhmmer Avg E-Value',
                          'Jackhmmer Avg Score', 'Jackhmmer % Genes Matched']

        for jackhammer_run, db in zip(['Marine', 'Other'], [self.marine_db, self.other_jackhmmer_db]):
            if db:
                column_order += [f'{jackhammer_run} {column}' for column in jackhmmer_cols]
            else:
                continue

        if hasattr(self, 'contains_refs'):
            column_order[5:5] = ref_columns

        columns_in_tbl = [col for col in column_order if col in self.feature_tbl]

        self.feature_tbl = self.feature_tbl[columns_in_tbl]  # There could be some dimension where order is different?

        # Save after final table
        if not self.advanced_fp.exists():
            with open(self.advanced_fp, 'wb') as advanced_fh:
                pickle.dump(self, advanced_fh)

    def run_prodigal(self):

        if not self.prodigal_aa_fp.exists():
            logger.info('Running prodigal on input sequences...')
            cmd = f'prodigal -i {self.nucleotide_fp} -o {self.prodigal_genes_fp} -a {self.prodigal_aa_fp} -p meta'
            execute(cmd, run_silent=True)
        else:
            logger.info('Re-using prodigal output from previous run')

    def build_table(self):
        logger.info('Generating basic feature table, features will be filled in as necessary...')
        genomes_len = {}
        genomes_name = {}

        for record in SeqIO.parse(self.nucleotide_fp, 'fasta'):
            genomes_len[record.id] = len(record.seq)
            genomes_name[record.id] = record.description

        # Predict proteins with Prodigal (will ignore if already exists)
        self.run_prodigal()

        # Build preliminary genomic features table
        gene_feats = {}

        for record in SeqIO.parse(self.prodigal_aa_fp, 'fasta'):
            gene = record.id
            cmpnts = record.description.split('#')
            start = cmpnts[1]
            end = cmpnts[2]
            strand = cmpnts[3]
            length = len(record.seq)

            gene_feats[len(gene_feats)] = {
                'Genome': record.id.rsplit('_', 1)[0],
                'CDS': gene,
                'Start': start,
                'End': end,
                'Strand': strand,
                'Length': length
            }

        gene_feats_df = pd.DataFrame().from_dict(gene_feats, orient='index')

        features = ['Name', 'Genes', 'Avg Gene Len', 'Gene Density', 'Strand Shifts']
        genome_feat = pd.DataFrame(index=gene_feats_df['Genome'].unique().tolist(), columns=features)

        for genome, genome_df in gene_feats_df.groupby(by='Genome'):

            num_cds = len(genome_df)
            avg_gene_len = genome_df['Length'].mean()
            gene_density = num_cds / (genomes_len[genome] / 1000)

            last_strand = None
            shifts = 0

            for index, series in genome_df.iterrows():
                strand = series['Strand']
                if not last_strand:
                    last_strand = strand
                    shifts += 0  # Yes, no change... but for mental assistance, it's needed
                else:
                    if strand != last_strand:
                        shifts += 1
                    else:
                        shifts += 0

            strand_shifts = shifts / num_cds
            # Don't forget name
            genome_name = genomes_name[genome]
            genome_feat.loc[genome, features] = [genome_name, num_cds, avg_gene_len, gene_density, strand_shifts]

        return genome_feat

    def add_references(self, known_archaeal: list, refseq_fp):

        # Load reference database
        refseq_virus_df = pd.read_csv(refseq_fp, delimiter='\t', header=0, index_col=0)

        self.refseq = refseq_virus_df[refseq_virus_df['Host'].isin(['bacteria', 'archaea'])]

        # Really wanted to avoid iterrows, but no quick way of re-finding the feature tables original hit to refseq
        matches = {}
        for index, df in self.feature_tbl.iterrows():
            match = self.refseq[self.refseq['Segmemts'].str.contains(index)]

            if not match.empty:
                matches[index] = {
                    'Host': match['Host'].values[0],
                    'Viral TaxID': match['TaxID'].values[0],
                    'Archaeal': 1 if match['Host'].values[0] == 'archaea' else 0
                }

        matches_df = pd.DataFrame().from_dict(matches, orient='index')

        self.feature_tbl = self.feature_tbl.merge(matches_df, how='left', left_index=True, right_index=True)

        # Quickly annotate known archaeal sequences
        self.feature_tbl.loc[self.feature_tbl.index.str.contains('|'.join(known_archaeal)), 'Archaeal'] = 1
        self.feature_tbl['Archaeal'].fillna(0, inplace=True)
        self.feature_tbl['Archaeal'] = self.feature_tbl['Archaeal'].astype(int)

        # Housekeeping - DON'T assume that any of the known archaeal virus sequences will have a matching refseq
        if 'Viral TaxID' in self.feature_tbl:
            self.feature_tbl['Viral TaxID'].fillna(0, inplace=True)
        else:
            self.feature_tbl['Viral TaxID'] = 0

        self.feature_tbl['Viral TaxID'] = self.feature_tbl['Viral TaxID'].astype(int)

    def run_hmmsearch(self, hmm_db: Path):

        aa_bp = self.prodigal_aa_fp.parent / self.prodigal_aa_fp.stem
        full_out = f'{aa_bp}.hmmscan.txt'

        if not all(os.path.exists(fp) for fp in [full_out, self.hmmscan]):
            rsearch_params = f'--cpu {cpu_count} --noali'
            search_cmd = f'hmmsearch {rsearch_params} --tblout {self.hmmscan} -o {full_out} {hmm_db} {self.prodigal_aa_fp}'
            execute(search_cmd)

    def parse_hmmsearch(self):
        logger.info('Parsing HMMsearch vs the pVOG database...')

        headers = ['target name', 'target accession', 'query name', 'query accession',
                   'full E-value', 'full score', 'full bias', 'single E-value', 'single score', 'single bias',
                   'exp', 'reg', 'clu', 'ov', 'env', 'dom', 'rep', 'inc', 'description of target']

        hmmscan = pd.read_csv(self.hmmscan, comment='#', sep='\s+', names=headers, header=None)

        # Get only "significant" matches
        hmmscan = hmmscan[hmmscan['full E-value'] < 1e-10]

        # Get genome
        hmmscan['genome'] = hmmscan['target name'].swifter.progress_bar(False).apply(lambda x: x.rsplit('_', 1)[0])

        return hmmscan

    def check_and_build_hmm_db(self):
        """
        Right now not dealing with building HMM profiles, only using them. As such, only need to press
        :return:
        """

        extensions = ['.h3m', '.h3i', '.h3f', '.h3p']

        hmm_fp = self.pvog_db

        if not all(Path(f'{hmm_fp}{ext}').exists() for ext in extensions):  # If source DB dir doesn't have everything
            logger.info('Copying original database data to output directory and processing...')
            new_hmm_fp = db_dir / self.pvog_db.name
            shutil.copy(hmm_fp, new_hmm_fp)
            build_cmd = f'hmmpress {new_hmm_fp}'
            execute(build_cmd)

            return new_hmm_fp

        return hmm_fp

    def check_and_build_mmseq_db(self, aa_fp: Path, db_fp: Path):
        # proteins, query_db, output_dir
        """
        Will only use (and therefore build) DB if db_fp not found
        :param aa_fp: Filepath to amino acid file that the database will be generated from
        :param db_fp:
        :return:
        """

        extensions = ['_h', '_h.dbtype', '.dbtype', '.index', '_h.index', '.lookup']

        if not all(Path(f'{db_fp}{ext}').exists() for ext in extensions):

            for ext in extensions:
                if not Path(f'{db_fp}{ext}').exists():
                    logger.info(f'Unable to identify {db_fp}{ext}. This is fine if this is the 1st run.')

            new_db_fp = self.db / db_fp.name

            build_cmd = f'mmseqs createdb {aa_fp} {new_db_fp}'
            execute(build_cmd, run_silent=True)

            return new_db_fp

        return db_fp  # Don't need to return anything different if the source DB directory has what we need

    def run_mmseq(self, query_db: Path, target_db: Path, out_db: Path, tmp: Path):

        if not Path(f'{out_db}.0').exists():
            logger.info('Running MMSeqs2 search...')
            run_cmd = f'mmseqs search {query_db} {target_db} {out_db} {tmp}'
            execute(run_cmd, run_silent=True)

        result_fp = Path(f'{out_db}.tab')
        if not result_fp.exists():
            convert_cmd = f'mmseqs convertalis {query_db} {target_db} {out_db} {result_fp}'
            execute(convert_cmd, run_silent=True)

        return result_fp

    def parse_mmseq(self, in_fp: Path, acc2taxid: Path):
        header = ['qaccver', 'saccver', 'pident', 'length', 'mismatch',
                  'gapopen', 'qstart', 'qend', 'sstart', 'send', 'evalue', 'bitscore']

        logger.info('Parsing MMSeqs2 results...')

        mmseq_df = pd.read_csv(in_fp, delimiter='\t', header=None, index_col=None, names=header)
        mmseq_df['genome'] = mmseq_df['qaccver'].swifter.progress_bar(False).apply(lambda x: x.rsplit('_', 1)[0])

        # Get only "significant" matches
        logger.info('Filtering for significant matches')
        mmseq_df = mmseq_df[mmseq_df['evalue'] < 1e-5]

        # Clean up accessions
        def get_accession(value):
            try:
                return value.split('.')[0]
            except:  # General, if it fails on a list for whatever reason, don't want that line
                return 'invalid'

        logger.info('Identifying accessions')
        mmseq_df['accession'] = mmseq_df['saccver'].swifter.progress_bar(False).apply(lambda x: get_accession(x))

        # Remove any bad lines
        mmseq_df = mmseq_df.loc[mmseq_df['accession'] != 'invalid']
        logger.info(f'There were {len(mmseq_df)} accessions processed')

        # Read in huge accession-to-taxid mapping
        now = time.time()

        if disable_vaex:

            if acc2taxid.suffix == '.gz':
                logger.info('Reading in Accession2TaxID (compressed) via pandas')
                acc2taxid_df = pd.read_csv(acc2taxid, delimiter='\t', header=0, index_col=None,
                                           dtype={'taxid': int}, compression='gzip')

            elif acc2taxid.suffix == '.accession2taxid':  # Uncompressed:
                logger.info('Reading in Accession2TaxID (non-compressed) via pandas')
                acc2taxid_df = pd.read_csv(acc2taxid, delimiter='\t', header=0, index_col=None,
                                           dtype={'taxid': int})

            elif acc2taxid.suffix == '.trimmed':  # Uncompressed, reduced:
                logger.info('Reading in trimmed Accession2TaxID (non-compressed) via pandas')
                acc2taxid_df = pd.read_csv(acc2taxid, delim_whitespace=True, header=0, index_col=None,
                                           dtype={'taxid': int, 'accession': str})  # delimiter=r"\s+"

            # TODO Can't handle trimmed.tar.gz, which is provided by new Zenodo

            else:
                acc2taxid_df = None
                logger.error('Unable to identify extension to taxonomy file. Ensure that the accession2taxid file '
                             'has either a *.gz extension OR is unzipped and uses accession2taxid.')
                exit(1)

        else:

            # Is there an hdf5 version of the accession2taxid file at its companion location?
            # TODO Simply allow user to specify THIS file, if an hdf file exists
            hdf5_acc2taxid_fp = f'{db_dir}/{acc2taxid.stem}.hdf5' \
                if acc2taxid.suffix == '.gz' else f'{db_dir}/{acc2taxid.name}.hdf5'

            hdf5_acc2taxid_fp = Path(hdf5_acc2taxid_fp)

            if not hdf5_acc2taxid_fp.exists():

                logger.info('Could not find an hdf5 version of Accession2TaxID file, preparing.... This may take '
                            'considerable time (20-30+ minutes) depending on your system.')

                if acc2taxid.suffix == '.gz':

                    with gzip.open(acc2taxid, 'rb') as acc2taxid_fh:
                        logger.info(f'Reading in Accession2TaxID (compressed) via vaex and saving to {hdf5_acc2taxid_fp}')
                        # https://vaex.io/docs/api.html#vaex.from_csv
                        acc2taxid_df = vaex.from_csv(acc2taxid_fh, delimiter='\t')

                elif acc2taxid.suffix == '.accession2taxid':  # Uncompressed

                    logger.info(f'Reading in Accession2TaxID (non-compressed) via vaex and saving to {hdf5_acc2taxid_fp}')
                    acc2taxid_df = vaex.from_csv(acc2taxid, delimiter='\t')

                elif acc2taxid.suffix == '.trimmed':  # Uncompressed, trimmed to only include 1st two columns

                    logger.info(f'Reading in trimmed Accession2TaxID (non-compressed) via vaex and saving to '
                                f'{hdf5_acc2taxid_fp}')
                    acc2taxid_df = vaex.from_csv(acc2taxid, delimiter=r"\s+")

                else:
                    acc2taxid_df = None
                    logger.error('Unable to identify extension to taxonomy file. Ensure that the accession2taxid file '
                                 'has either a *.gz extension OR is unzipped and uses accession2taxid.')
                    exit(1)

                acc2taxid_df.export_hdf5(hdf5_acc2taxid_fp)

            else:
                logger.info('Opening existing hdf5 version of Accession2TaxID...')
                acc2taxid_df = vaex.open(hdf5_acc2taxid_fp, progress=True)

        post_read = time.time() - now
        logger.info(f'It took {post_read} seconds to process Accession2TaxID file')

        # With Accession2TaxID loaded as a vaex dataframe, can continue to filtering
        logger.info('Appending taxonomy to accessions of MMSeqs results...')

        # https://medium.com/bigdatarepublic/advanced-pandas-optimize-speed-and-memory-a654b53be6c2
        # https://stackoverflow.com/questions/40860457/improve-pandas-merge-performance

        # Merging on accessions can take well over an hour (or 2) on 28 cores, so pre-filter to reduce load
        logger.info('Pre-filtering large taxonomy database...')

        # if disable_vaex:
        col_list = acc2taxid_df.columns.tolist() if disable_vaex else acc2taxid_df.get_column_names(hidden=True)
        if 'accession' in col_list:
            filtered_acc2taxid_df = acc2taxid_df[acc2taxid_df['accession'].isin(mmseq_df['accession'])]
        elif 'accession.version' in col_list:
            filtered_acc2taxid_df = acc2taxid_df[acc2taxid_df['accession.version'].isin(mmseq_df['accession'])]
        else:
            error(f'Unable to identify an accession column in {acc2taxid}, {acc2taxid_df}')

        # else:  # Vaex object
        #     print(acc2taxid_df.get_column_names(hidden=True))
        #     filtered_acc2taxid_df = acc2taxid_df[acc2taxid_df['accession.version'].isin(mmseq_df['accession'])]

        acc_starttime = time.process_time()
        if enable_gpu:
            logger.debug('Converting accessions dataframe to cuDF...')
            gpu_acc2taxid_df = cudf.DataFrame.from_pandas(filtered_acc2taxid_df)  # TODO Check if possible
            logger.debug('Converting MMSeqs2 dataframe to cuDF...')
            gpu_mmseq_df = cudf.DataFrame.from_pandas(mmseq_df)

            logger.debug('Merging...')
            mmseq_df = gpu_mmseq_df.merge(gpu_acc2taxid_df, on=['accession'], how='left')

        else:
            if not disable_vaex:
                if isinstance(filtered_acc2taxid_df, vaex.dataframe.DataFrameLocal):
                    filtered_acc2taxid_df = filtered_acc2taxid_df.to_pandas_df()

            filtered_acc2taxid_df.set_index('accession', inplace=True)
            mmseq_df.set_index('accession', inplace=True)
            mmseq_df = mmseq_df.join(filtered_acc2taxid_df, how='left')

            mmseq_df = mmseq_df.reset_index()

        acc_end_time = time.process_time() - acc_starttime
        logger.info(f'It took {acc_end_time} seconds to append taxonomy')

        mmseq_df.to_csv(self.mmseqs, sep=',', index=True)
        logger.info(f'Wrote MMseqs2 CSV to {self.mmseqs}')

    def runner_jackhmmer(self, faa_db, kind):

        aa_bp = self.prodigal_aa_fp.parent / f'{self.prodigal_aa_fp.stem}.{kind}'
        full_out = Path(f'{aa_bp}.jackhmmer.txt')
        aln_out = Path(f'{aa_bp}.jackhmmer.aln')
        tbl_out = Path(f'{aa_bp}.jackhmmer.tbl')
        domain_tbl_out = Path(f'{aa_bp}.jackhmmer.domain.tbl')

        if not all(fp.exists() for fp in [full_out, aln_out, tbl_out, domain_tbl_out]):
            run_opts = f'-T 20 --domT 20 --incT 20 --incdomT 20 --wnone --cpu {cpu_count}'
            cmd = f'jackhmmer -o {full_out} -A {aln_out} --tblout {tbl_out} --domtblout {domain_tbl_out} {run_opts} {faa_db} {self.prodigal_aa_fp}'
            execute(cmd)

    def parse_jackhmmer(self, kind):

        logger.info(f'Parsing {kind} Jackhmmer vs archaeal pVOGs + environmental DBs')

        header = ['target name', 'target accession', 'query name', 'query accession', 'E-value (full)', 'score (full)',
                  'bias (full)', 'E-value (best 1 domain)', 'score (best 1 domain)', 'bias (best 1 domain)', 'exp',
                  'reg',
                  'clu', 'ov', 'env', 'dom', 'rep', 'inc', 'description of target']

        jackhmmer_fp = self.marine_jackhmmer if kind == 'Marine' else self.other_jackhmmer

        if not Path(jackhmmer_fp).exists():
            return pd.DataFrame()

        jackhmmer_df = pd.read_csv(jackhmmer_fp, comment='#', sep='\s+', header=None, index_col=None,
                                   names=header)

        jack_start = time.process_time()
        jackhmmer_df['genome'] = jackhmmer_df['target name'].apply(lambda x: x.rsplit('_', 1)[0])  # Yes, target is the
        # query sequence
        jack_end = time.process_time() - jack_start
        logger.info(f'It took {jack_end} seconds to complete')
        jackhmmer_df['genome'] = jackhmmer_df['target name'].swifter.progress_bar(False).apply(lambda x: x.rsplit('_', 1)[0])
        jack_final = time.process_time() - jack_end
        logger.info(f'It took {jack_final} seconds to complete with swifter')
        # mmseq_df['accession'] = mmseq_df['saccver'].swifter.progress_bar(False).apply(lambda x: get_accession(x))

        # Get only "significant" matches
        jackhmmer_df = jackhmmer_df[jackhmmer_df['E-value (full)'] < 1e-5]

        return jackhmmer_df

    def save_csv(self, out_fp):
        self.feature_tbl.to_csv(out_fp, index=True, sep=',')


def parse_taxid(taxid):

    if taxid == '-1':  # Need to get a better handle on this!
        return 'Unknown'

    try:
        lineage = ncbi.get_lineage(taxid)
    except:  # If there's ANY issue with getting the lineage, I don't want it
        return 'Unknown'
        # lineage = [1, 10239]  # Need to fix this too!

    names = ncbi.get_taxid_translator(lineage)

    sorted_ranked = {}
    for id_, lineage_name in names.items():
        rank_name = ncbi.get_rank([id_])
        if list(rank_name.values())[0] != 'no rank':
            sorted_ranked[list(rank_name.values())[0]] = lineage_name

    try:
        superkingdom = sorted_ranked['superkingdom']
    except KeyError:
        return np.nan

    return superkingdom.lower()


def proximityMatrix(model, XX, normalize=True):
    # https://stackoverflow.com/a/47907919
    terminals = model.apply(XX)
    nTrees = terminals.shape[1]

    a = terminals[:, 0]
    proxMat = 1*np.equal.outer(a, a)

    for i in range(1, nTrees):
        a = terminals[:, i]
        proxMat += 1*np.equal.outer(a, a)

    if normalize:
        proxMat = proxMat / nTrees

    return proxMat


if __name__ == "__main__":

    # Globals
    cpu_count = args.cpu_count
    logger.info(f'Identified {cpu_count} CPUs.')

    # For initial round of testing, tools must exist in user's PATH
    progs = ['prodigal', 'jackhmmer', 'mmseqs', 'hmmsearch']
    for prog in progs:
        if not shutil.which(prog):
            error(f'Unable to identify {prog}, please ensure that it is in your $PATH to continue.')

    # Create output directory
    output_dir = Path(args.output_dir)
    if not output_dir.exists():
        logger.info(f'Creating directory: {output_dir}')
        os.mkdir(output_dir)
    else:
        logger.info(f'Re-using existing directory: {output_dir}')

    # Create database directories, if necessary
    if args.dbs_dir:  # There's something specified
        dbs_dir = Path(args.dbs_dir)
        if dbs_dir.is_dir():  # It's a directory
            all_vogs = [f'AllvogHMMprofiles.hmm.{x}' for x in ['h3f', 'h3i', 'h3m', 'h3p']]
            db_files = ['nr.faa', 'VOGTable.txt', 'viruses.txt', 'ViralQuotient.txt',
                        'AllvogHMMprofiles.hmm', 'pVOG_prots_ref_marine_pVOG.faa']

            # TODO Refactor found/not-found in DB, then found/not-found by specific
            for db_file in db_files:
                fp = dbs_dir / db_file
                if not fp.exists():
                    # Check if it's already provided
                    if db_file == 'nr.faa':
                        if args.nr_db:  # If exists, or was provided
                            if Path(args.nr_db).exists():
                                logger.info(f'Not found in DB directory, but specified individually')
                                continue
                            else:
                                error(f'Could not identify {db_file}. Please check to see if you have the appropriate'
                                      f' file in the DB directory, or specify it individually.')
                        else:
                            error(f'Could not identify {db_file}. Please check to see if you have the appropriate'
                                  f' file in the DB directory, or specify it individually.')

                    elif db_file == 'viruses.txt':
                        if args.refseq_viruses:
                            if Path(args.refseq_viruses).exists():
                                logger.info(f'Not found in DB directory, but specified individually')
                                continue
                            else:
                                error(f'Could not identify {db_file}. Please check to see if you have the appropriate'
                                      f' file in the DB directory, or specify it individually.')
                        else:
                            error(f'Could not identify {db_file}. Please check to see if you have the appropriate'
                                  f' file in the DB directory, or specify it individually.')

                    elif db_file in ['VOGTable.txt', 'ViralQuotient.txt']:
                        if args.pvog_dir:
                            pvog_dir_dir = Path(args.pvog_dir)
                            if pvog_dir_dir.is_dir():
                                if (pvog_dir_dir / db_file).exists():
                                    logger.info(f'Not found in DB directory, but specified individually')
                                    continue
                                else:
                                    error(f'Unable to find {db_file} in {pvog_dir_dir}.')
                            else:
                                error(f'{pvog_dir_dir} does not appear to be a directory. Please specify a directory, '
                                      f'not a path to a file within that directory')
                        else:
                            error(f'Could not identify {db_file}. Please check to see if you have the appropriate'
                                  f' file in the DB directory, or specify it individually.')

                    elif db_file == 'pVOG_prots_ref_marine_pVOG.faa':
                        if args.marine_jackhmmer_db:
                            if Path(args.marine_jackhmmer_db).exists():
                                logger.info(f'Not found in DB directory, but specified individually')
                                continue

                            else:
                                error(f'Could not identify {db_file}. Please check to see if you have the appropriate'
                                      f' file in the DB directory, or specify it individually.')

                        else:
                            error(
                                f'Could not locate {db_file} in {str(dbs_dir)}, nor was it found specified individually'
                                f' (i.e. --db-nr or --viral-refseq-txt)')

                    elif db_file == 'AllvogHMMprofiles.hmm':
                        if args.pvog_db:  # If provided

                            if Path(args.pvog_db).exists():
                                logger.info(f'Not found in DB directory, but specified individually')
                                continue

                            # TODO Need to validate presence of all HMM profiles
                            # pvog_db_dir = Path(args.pvog_db)
                            # if pvog_db_dir.is_dir():
                            #     if (pvog_db_dir / db_file).exists():
                            #         logger.info(f'Not found in DB directory, but specified individually')
                            #         continue
                            #     else:
                            #         error(f'Unable to find {db_file} in {pvog_db}.')
                            # else:
                            #     error(f'{pvog_db} does not appear to be a directory. Please specify a directory, '
                            #           f'not a path to a file within that directory')
                            else:
                                error(f'Could not identify {db_file}. Please check to see if you have the appropriate'
                                      f' file in the DB directory, or specify it individually.')
                        else:
                            error(f'Could not identify {db_file}. Please check to see if you have the appropriate'
                                  f' file in the DB directory, or specify it individually.')

                    else:
                        error(f'Could not locate {db_file} in {str(dbs_dir)}, nor was it found specified individually'
                              f' (i.e. --db-nr or --viral-refseq-txt)')

                else:  # Else it's found...  # TODO Possibly don't write over argparse
                    if db_file == 'nr.faa':
                        args.nr_db = fp
                    elif db_file == 'viruses.txt':
                        args.refseq_viruses = fp
                    elif db_file in ['VOGTable.txt', 'ViralQuotient.txt']:
                        args.pvog_dir = dbs_dir
                    elif db_file == 'AllvogHMMprofiles.hmm':
                        args.pvog_db = fp
                    elif db_file == 'pVOG_prots_ref_marine_pVOG.faa':
                        args.marine_jackhmmer_db = fp
                    else:
                        error('')

                    logger.info(f'Identified {db_file} in {dbs_dir}')

    db_dir = output_dir / 'databases'
    if not db_dir.exists():
        logger.info(f'Creating database directory: {db_dir}')
        os.mkdir(db_dir)
    else:
        logger.info(f'Re-using existing database directory: {db_dir}')

    # Keep track of files that need to be opened/used throughout
    root_name = Path(args.input_fp).stem
    data = {
        'Basename': root_name,
        'Databases': db_dir,
        'Database-NR': Path(args.nr_db),
        'Database-Marine': Path(args.marine_jackhmmer_db),
        'Database-pVOG_HMMs': Path(args.pvog_db),
        'Database-Accession2TaxID': Path(args.accession2tax),  # Still must be specified
        'Database-pVOGs': Path(args.pvog_dir),
        'Nucleotide': Path(args.input_fp),
        'Prodigal Genes': output_dir / f'{root_name}.genes',
        'Prodigal AA': output_dir / f'{root_name}.faa',
        'Features': output_dir / 'features_basic.pkl',
        'Features-Advanced': output_dir / 'model_features_advanced.pkl',
        'HMMScan': output_dir / f'{root_name}.hmmscan.tbl',
        'MMSeqs2': output_dir / f'{root_name}.MMSeq2.csv',
        'Marine Jackhmmer': output_dir / f'{root_name}.marine.jackhmmer.tbl',  # Still must be specified
        'Other Jackhmmer': output_dir / f'{root_name}.other.jackhmmer.tbl',
        'Build Models': args.build_models,
        'Output Directory': output_dir
    }

    # Input sequences + copy
    input_fp = output_dir / Path(args.input_fp).name
    if not input_fp.exists():
        logger.info('Copying input file to output directory...')
        shutil.copy(args.input_fp, input_fp)
    else:
        logger.info('Re-using input file within output directory...')

    advanced_table_fp = data['Features-Advanced']
    basic_table_fp = data['Features']

    if advanced_table_fp.exists():  # If advanced, then ready to make model or predict
        with open(advanced_table_fp, 'rb') as advanced_table_fh:
            logger.info('Loading nigh-complete Feature Table for subsequent model building or predictions')
            feature_table = pickle.load(advanced_table_fh)

    else:  # Need to start from scratch
        # At least don't need to ask about building models
        logger.info('Unable to find Feature Table (this is fine), building from scratch...')
        feature_table = FeatureTable(data, add_refs=args.build_models)

    feature_table.feature_tbl.fillna(0, inplace=True)

    if args.build_models:

        # Remove co-correlations and unimportant columns
        columns = ['Archaeal', 'Name', 'Genes', 'Viral TaxID', 'Host', 'Jackhmmer No Matches',
                   'pVOG Total Hits', 'pVOG Unique Hits']  # 'MMSeqs2 Avg viruses (archaea) E-Value' not found in axis?
        columns = [col for col in columns if col in feature_table.feature_tbl]
        # pVOG Unique Hits > 0.95 w/ pVOG Bacteria
        # pVOG Bacteria > 0.95 w/ pVOG Total Hits
        # Jackhmmer % Genes Matched > 0.95 w/ Jackhmmer No Matches
        # 'MMSeqs2 Avg viruses (archaea) Score' > 0.95 MMSeqs2 Avg viruses (archaea) E-Value

        pre_model_table = feature_table.feature_tbl

        y = feature_table.feature_tbl.Archaeal
        X = feature_table.feature_tbl.drop(columns=columns)

        # Split test data into 70% training and 30% test (though some argue 80:20 is more appropriate)
        x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

        # Going to use recursive feature elimination with cross validation and random forest
        # Not doing Univariate feature selection - don't want to try all k

        rf_classifier = RandomForestClassifier(random_state=43, n_estimators=100)  # Maybe 1000?
        rf_classifier = rf_classifier.fit(x_train, y_train)

        # Determine feature importances in rf
        importance = rf_classifier.feature_importances_
        std = np.std([tree.feature_importances_ for tree in rf_classifier.estimators_], axis=0)
        indices = np.argsort(importance)[::-1]

        plt.figure(figsize=(14, 13))
        plt.title('Feature Importances')
        plt.xlabel('Features')
        plt.ylabel('Feature Importance')
        plt.bar(range(x_train.shape[1]), importance[indices], color="g")  # yerr=std[indices], align="center")

        plt.xticks(range(x_train.shape[1]), x_train.columns[indices], rotation=90)
        plt.xlim([0, x_train.shape[1]])
        plt.savefig(output_dir / 'Random_Forest-Feature_Importances.png', dpi=600, bbox_inches='tight')
        plt.clf()

        # Add in cross-validation
        rfe_with_cv = RFECV(estimator=rf_classifier, step=1, cv=5, scoring='accuracy')  # 5-fold cross-validation
        rfe_with_cv = rfe_with_cv.fit(x_train, y_train)
        acc = accuracy_score(y_test, rfe_with_cv.predict(x_test))

        logger.info(f'Accuracy with Classifier (Random Forest) is: {acc*100}')
        logger.info(f'Optimal number of features: {rfe_with_cv.n_features_in_}')
        features_list = '\n\t'.join(x_train.columns[rfe_with_cv.support_].tolist())
        logger.info(f'Best features: {features_list}')

        # Features vs Cross-Validation
        plt.figure(figsize=(14, 13))
        plt.xlabel('Number of features selected')
        plt.ylabel('Cross validation scorer of number of selected features')
        # grid_scores_ depreciated in 0.23 and removed in 0.25 # https://github.com/scikit-learn/scikit-learn/pull/20161
        plt.plot(range(1, len(rfe_with_cv.cv_results_['mean_test_score']) + 1),
                 rfe_with_cv.cv_results_['mean_test_score'])
        plt.savefig(output_dir / 'Random_Forest-Cross_Validation_Scores.png', dpi=600, bbox_inches='tight')
        plt.clf()

        # Save everything
        model_fp = output_dir / 'rf_model.pkl'
        joblib.dump(rfe_with_cv, model_fp)

        model_columns_fp = str(data['Features-Advanced']).replace('_features_advanced', '_predict_columns')
        with open(model_columns_fp, 'wb') as model_columns_fh:
            pickle.dump(x_train.columns.tolist(), model_columns_fh)

        # Get proximity matrix
        proximity_matrix = proximityMatrix(rf_classifier, x_train, normalize=True)
        training_index = x_train.index.tolist()
        proximity_df = pd.DataFrame(data=proximity_matrix, index=training_index, columns=training_index)

        # Save proximity matrix
        proximity_df.to_csv(output_dir / 'model_proximity_matrix.csv')

        # Save pre-model table
        pre_model_table.to_csv(output_dir / 'model_premodel_table.csv')

    else:
        model_fp = args.model_fp
        rf_classifier = joblib.load(model_fp)

        # Need to ensure that the model and prediction share the same columns
        # If the feature table DOESN'T have
        model_columns_fp = model_fp.replace('rf_model', 'model_predict_columns')
        with open(model_columns_fp, 'rb') as model_columns_fh:
            model_columns = pickle.load(model_columns_fh)

        # Also need to ensure that all the columns in the prediction are ALSO IN model
        not_in_prediction = set(model_columns) - set(feature_table.feature_tbl.columns.tolist())

        if not_in_prediction:  # There are columns not in there!
            for not_in in not_in_prediction:
                feature_table.feature_tbl[not_in] = 0  # Need to ensure that model columns are filled out

        # "Drop" columns that weren't in the model
        feature_table.feature_tbl = feature_table.feature_tbl[model_columns]

        X = feature_table.feature_tbl

        predict = rf_classifier.predict(X)
        probs = rf_classifier.predict_proba(X)

        prediction = list(map(bool, predict))
        feature_table.feature_tbl['Prediction'] = predict

        feature_table.feature_tbl['NOT Archaeal Virus Probability'] = [prob.tolist()[0] for prob in probs]
        feature_table.feature_tbl['Archaeal Virus Probability'] = [prob.tolist()[1] for prob in probs]

        feature_table_with_pred_fp = output_dir / 'feature_table_with_predictions.csv'
        feature_table.feature_tbl.to_csv(feature_table_with_pred_fp)

        logger.info(f'Feature table with predictions saved to {feature_table_with_pred_fp}')
