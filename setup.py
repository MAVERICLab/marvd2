from os import path
from setuptools import setup
from __version__ import version

curr_dir = path.abspath(path.dirname(__file__))

# https://docs.python.org/3/distutils/sourcedist.html
# https://github.com/pypa/sampleproject/blob/master/setup.py
# noinspection PyPackageRequirements
setup(
    name='MArVD2',
    version=version,
    description='Metagenomic Archaeal Virus Detector 2',
    license='GPL-3',
    url='https://bitbucket.org/MAVERICLab/marvd2/',
    author='Benjamin Bolduc, Dean Vik',
    author_email='bolduc.10@osu.edu',
    keywords=['iVirus', 'virus', 'metagenomics', 'virus identification'],
    python_requires='>=3.9,<3.10.0a0',
    # https://packaging.python.org/discussions/install-requires-vs-requirements/
    install_requires=[
        'biopython==1.80',  # Sync with Bioconda
        'ete3>=3.1.2',
        'psutil>=5.9.4',
        'swifter>=1.3.4',
        'joblib>=1.2.0',
        'matplotlib>=3.4.3',
        'numpy>=1.23.5,<2.0a0',
        'pandas>=1.5.2',
        'scikit-learn>=1.1.2,<1.9.1',  #
        'scipy>=1.10.0',  # note: is this required?
        'prodigal==2.6.3',
        'hmmer>=3.3.2',
        'mmseqs2>=13.45111',
        # 'vaex'
    ],
    project_urls={
        'Bug Reports': 'https://bitbucket.org/MAVERICLab/marvd2/issues',
        'Funding': '',
        'Source': 'https://bitbucket.org/MAVERICLab/marvd2/'
    },
    classifiers=[
        "Intended Audience :: Science/Research",
        "Natural Language :: English",
        "Topic :: Scientific/Engineering :: Bio-Informatics",
        "License :: OSI Approved :: GPL3 License",
        "Programming Language :: Python :: 3",
    ],
    scripts=['MArVD2.py']
)
