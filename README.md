MArVD2: Metagenomic Archaeal Virus Detector
====================================================

MArVD2 is a tool to identify archaeal viruses from a set of viral genomes or genome fragments.

## General installation and run requirements

MArVD2 requires

1. The MArVD2.py code itself
2. A set of databases for feature table generation
3. A random forest model for archaeal virus prediction
4. Only if you are generating a new model and not using the one provided here, a adequate training dataset.

Details below.

## Specific tool requirements

MArVD2 requires numerous python packages to function correctly, and each must be properly installed and working for 
MArVD2 to also work. Depending on your system, several of these may need to be installed into the conda environment 
you create for MArVD2 as detailed below. If specific versions of the below are prompted during MArVD2 installation, 
please also install these with conda.

 * python 3.9
 * numpy
 * scikit-learn
 * pandas
 * psutil
 * ETE3
 * swifter
 * matplotlib
 * biopython
 * vaex (now optional!)

MArVD2 requires several executables, depending on use. Generally, you want these tools to be in your system or user PATHs.

 * [Prodigal](https://github.com/hyattpd/Prodigal)
 * Jackhmmer (from the [Hmmer3](http://hmmer.org/) package)
 * [MMSeqs2](https://github.com/soedinglab/MMseqs2)

## Installation

### Anaconda - Recomended

To install, we'll use [Anaconda](https://anaconda.org/) and [Mamba](https://mamba.readthedocs.io/en/latest/index.html). 
Anaconda for its ease in installing python packages, and mamba, for handling installation quickly. Be sure that you have a version of miniconda that pins python 3.9
such as https://repo.anaconda.com/miniconda/Miniconda3-py39_23.3.1-0-Linux-x86_64.sh

```bash
conda create -n MArVD2 python=3.9
conda activate MArVD2
conda install -c conda-forge mamba
mamba install -c bioconda -c conda-forge "marvd2==0.11.9"
```

To speeds up taxonomy processing (which we strongly recommend as this tends to be the processing bottleneck), you'll need to additionally install:

```bash
mamba install -y -c conda-forge vaex
```

### Singularity/Apptainer

We also provide a Singularity/Apptainer definition file.

To build
```bash
sudo apptainer build MArVD2.sif MArVD2.def
```

Then to run
```bash
apptainer run MArVD2.sif <cmd>
```

If you haven't upgraded to Apptainer from Singularity, simply replace "apptainer" with "singularity"

### Bleeding Edge

It can take some time (even days) to update the Bioconda channel with new versions of MArVD2. If you want the latest 
version, you can run the following.

```bash
conda create -n MArVD2 python=3.9
conda activate MArVD2
conda install -y -c conda-forge mamba
mamba install -y -c bioconda marvd2
git clone https://bitbucket.org/MAVERICLab/marvd2.git
cd marvd2
python -m pip install . --no-deps
```

## Databases

Databases must also be downloaded and their path(s) specified on the command line for MArVD2 to work. 
These databases include:

 * Archaeal virus and Phage sequences from NCBI nr.
 * The pVOGs database.
 * A custom database of marine archaeal viruses, the OcAVdb.

There are two ways to specify the database files for MArVD2.

1. Download the Zenodo or Cyverse version (below) and extract it to a directory and specify the directory on the command line
2. Manually selecting the location for each file

### Zenodo

We have a DOI version of these datasets and the model available from Zenodo [DOI-versioned (10.5281/zenodo.7768113)](https://doi.org/10.5281/zenodo.7768113) 

The files are available by either curl or wget: 

```bash
wget https://zenodo.org/record/7768113/files/MArVD2_files.tar.gz
```

Then decompress using tar:

```bash
tar xf MArVD2_files.tar.gz
```

### On CyVerse

If you have access to CyVerse, a full list of all databases used in the MArVD2 manuscript is available under the 
DOI: [10.25739/1ttq-2q60](https://dx.doi.org/10.25739/1ttq-2q60)

All databases and training/benchmarking datasets are available at CyVerse:

https://de.cyverse.org/data/ds/iplant/home/shared/commons_repo/curated/DeanVik_MArVD2_Apr2022

### Manual 

Individual files can be downloaded using the instructions below.

#### Viral Refseq

```bash
wget ftp.ncbi.nlm.nih.gov/genomes/GENOME_REPORTS/viruses.txt
```

#### pVOGs

There are three files required. VOGTable.txt and AllvogHMMprofiles.tar.gz are available at the pVOGs download page 
from NCBI. ViralQuotient.txt is no longer available from the pVOGs website, so is included here.

```bash
wget https://ftp.ncbi.nlm.nih.gov/pub/kristensen/pVOGs/downloads/VOGTable.txt
wget https://ftp.ncbi.nlm.nih.gov/pub/kristensen/pVOGs/downloads/All/AllvogHMMprofiles.tar.gz 
tar xf AllvogHMMprofiles.tar.gz
find AllvogHMMprofiles/ -name "*.hmm" -exec cat '{}' ";" > AllvogHMMprofiles.hmm
```

#### NR & Accession to TaxID file(s)

These can be found at NCBI:

```bash
wget https://ftp.ncbi.nlm.nih.gov/blast/db/FASTA/nr.gz
wget https://ftp.ncbi.nlm.nih.gov/pub/taxonomy/accession2taxid/prot.accession2taxid.gz
```

The file does not need to be decompressed.

#### Marine Jackhmmer database

This can only be downloaded from [CyVerse](https://datacommons.cyverse.org/browse/iplant/home/shared/commons_repo/curated/DeanVik_MArVD2_Apr2022/databases/pVOG_prots_ref_marine_pVOG.faa.gz) 
OR from [Zenodo](https://zenodo.org/record/7768113/files/MArVD2_files.tar.gz)

## Pre-developed model

The final thing that MArVD2 needs to predict archaeal viruses is a model developed from the provided training data. We have included the model used to benchmark MArVD2
in each of the Zenodo and Cyverse repositories above or it can be directly cloned from this repository. We suggest downloading this file by:

```bash
git clone https://bitbucket.org/MAVERICLab/marvd2.git
```
or 

```bash
wget https://zenodo.org/record/7768113/files/MArVD2_files.tar.gz
```

This model can then be specified by command line when running MArVD2 (see below). Optionally, if users have an updated version of a training database that they 
would like to use, they may create a new model using the ```--build-models``` option below, however, if this approach is taken, we cannot verify that the benchmarking
statistics presented by our pre-developed model will be equivalent.

To test your installation of MArVD2 for prediction please use as input the "toy_dataset.fasta" from Zenodo as above. You can also test building a new model using the 
"toy_build_dataset.fasta" dataset.

## Running MArVD2

MArVD2 runs in two phases. The 1st phase is creating a model using a training dataset with known archaeal virus genomes (this is optional, we provide a tested functional model).
These sequences can be anything, as long as they have a specific term in the sequence header that distinguishes the archaeal viruses from other 
viruses, see "-a" below. 

Again for most users, we provide the model used to benchmark MArVD2 in the above repositories and 
we suggest using this unless you know what you are doing. 

The 2nd phase is to apply that model to new sequences.

### General Usage

Assuming you have cloned this repository and followed the above instructions, for the general user just predicting archaeal viruses will be done by (adjusting paths to respective files):
Note that the Zenodo package comes with two versions of the prot.accession2taxid file. Unless you are on a system with a large memory capacity, use the trimmed version of this file.

```bash
MArVD2.py -i <your-input.fasta> --load-model rf_model.pkl -o <your_prediction_dir> --db-pvog AllvogHMMprofiles.hmm --db-nr nr.faa --marine-jackhmmer-db pVOG_prots_ref_marine_pVOG.faa --viral-refseq-txt viruses.txt --pvog-dir other_dbs --db-accession2tax prot.accession2taxid.trimmed
```

There is one main output from the this, "feature_table_with_predictions.csv". See below for a more detailed 
explanation of outputs.

To build a new model if you are confident that you have an appropriate dataset:

```bash
MArVD2.py -i <training_dataset.fasta> --build-models -a <archaeal_virus_keyword> -o <training> --db-pvog AllvogHMMprofiles.hmm --db-nr nr.faa --marine-jackhmmer-db pVOG_prots_ref_marine_pVOG.faa --viral-refseq-txt viruses.txt --pvog-dir other_dbs --db-accession2tax prot.accession2taxid.trimmed
```

In either case, if you choose to, you may include other reference databases with the "--other-jackhmmer-db" option, however this should only be done if you are confident that the represented viruses
are indeed archaeal viruses.

After the training is complete, several outputs will be generated. The most important are the two *.pkl files, 
rf_model.pkl and model_predict_columns.pkl.

A full list of options is available by:

```bash
MArVD2.py -h
```
Users no longer have to specify all database. Instead the directory containg all of the databases can be specified with ```--dbs-dir```.
The ecxeption to this is the accession2taxid NCBI taxonomy file which still must be specified.

On a system with 40 192GB memory CPUs, MArVD2 memory and time usage is as follows:

To build the model with the described training dataset - ~1GB memory and 158 CPU hours
To run a prediction with the toy dataset (only 4 sequences) - ~58GB memory and 10 CPU hours
To run a prediction with the 6117 archaeal virus sequences from JGI/VR v5.1. - ~70GB memory and ~466 CPU hours  


### On OSC

If you have access to the [Ohio Supercomputer Center](https://www.osc.edu/) and the Sullivan Lab's project, we'll first load the tool:

```bash
module use /fs/project/PAS1117/modulefiles
module load MArVD2/0.11.4
```

## Parameters:

```bash
usage: MArVD2.py [-h] [-i FILEPATH] [-m FILEPATH] [--build-models]
                 [-o DIRPATH] [-a ARCHAEALS] [--dbs-dir DIRPATH]
                 [--db-pvog FILEPATH] [--db-nr FILEPATH]
                 [--db-accession2tax FILEPATH]
                 [--marine-jackhmmer-db FILEPATH]
                 [--other-jackhmmer-db FILEPATH] [--viral-refseq-txt FILEPATH]
                 [--pvog-dir DIRPATH] [--no-dbs] [--num-features NUM_FEATURES]
                 [--enable-gpu] [-c CPU_COUNT] [--reload-taxonomy]
                 [--disable-vaex]

MArvD2 is a tool to classify the enigmatic archaeal viruses.

optional arguments:
  -h, --help            show this help message and exit

Input and Outputs:
  -i FILEPATH, --input-fp FILEPATH
                        Input sequences/contigs/genomes to predict.
  -m FILEPATH, --load-models FILEPATH
                        Trained model to use.
  --build-models        Enable to generate models based on input data. MUST
                        include archaeal terms.
  -o DIRPATH, --output-dir DIRPATH
                        Output directory where all results will be stored.
  -a ARCHAEALS, --archaeal-terms ARCHAEALS
                        Terms to use for classifying sequence as an archaeal
                        virus. Use this repeatedly to denote multiple terms,
                        e.g. -a ETNP -a archaea -a portal_prot5
  --dbs-dir DIRPATH     Directory with ALL database files. MArVD2 will assume
                        that all DB files are in this directory unless
                        specified individually.

Databases and files:
  --db-pvog FILEPATH    Location of pVOG HMM profiles. MUST have the *.hmm
                        extension. Can be downloaded from: https://ftp.ncbi.nl
                        m.nih.gov/pub/kristensen/pVOGs/downloads.html
  --db-nr FILEPATH      Location of the NR database. MUST be in AMINO ACID
                        format and have the *.faa extension.
  --db-accession2tax FILEPATH
                        Location of the prot.accession2taxid data file,
                        downloaded from the NCBI taxonomy database and
                        subsequently parsed to remove several columns to
                        minimize disk space.
  --marine-jackhmmer-db FILEPATH
                        Location of the pVOG+environmental archaea database.
                        MUST be in AMINO ACID format and have the *.faa
                        extension.
  --other-jackhmmer-db FILEPATH
                        (Optional) Location of the pVOG+other archaea
                        database. MUST be in AMINO ACID format and have the
                        *.faa extension.
  --viral-refseq-txt FILEPATH
                        Location of the viruses.txt file of viral refseq. Can
                        be downloaded from ftp.ncbi.nlm.nih.gov/genomes/GENOME
                        _REPORTS/viruses.txt
  --pvog-dir DIRPATH    Location of the pVOG data DIRECTORY. Needs to contain
                        the VOGTable.txt and ViralQuotient.txt files. Can be
                        downloaded from
                        https://ftp.ncbi.nlm.nih.gov/pub/kristensen/pVOGs/
  --no-dbs              Skip processing related to database searches. This
                        will assume output results files from the searches are
                        present in the output directory

Misc Options:
  --num-features NUM_FEATURES
                        Number of features to use in either a straight-up
                        classification, or maximum value to use for recursive
                        feature elimination.
  --enable-gpu          Enable GPU acceleration (IF POSSIBLE). If GPUs are not
                        available, then this option will not be activated.
  -c CPU_COUNT, --cpu-count CPU_COUNT
                        Number of CPUs to use during processing. Not all parts
                        of MArVD2 are multi-threaded.
  --reload-taxonomy     Update ETE3's database and exit.
  --disable-vaex        Disable the use of Vaex. This will not generate a HDF
                        file and instead use Pandas to directly read the input
                        file(s).

```

## Output

The key output file from "--load-model" is the "feature_table_with_predictions.csv" which provides metrics for each of the features MArVD2 tests, as well as a 
"Prediction" (1 for archaeal virus, 0 for non-archaeal virus) and an "Archaeal Virus Probability" and "NOT Archaeal Virus Probability" score. 
Also provided are (i) the prodigal results in a .faa (proteins), .fna (ORFs), and .genes (ORF prediction) file, (ii) the results from the hmmscan in a "hmmscan.tbl" and 
hmmscan.txt" file, (iii) the results from the jackhmmer search in a "jackhmmer.aln", "jackhmmer.domain.tbl", "jackhmmer.tbl", and "jackhmmer.txt" file, (iv) the results
from the MMseqs2 search in a "MMSeq2.csv" file, and (v) the index files for each of the databases. 

The output from "--build-model" is very similar except that the key output file, in this case, is the "rf_model.pkl" which is a pickle file for the actual model.
In this instance, we are also provided with some summary model build statistics in the "Random_Forest-Cross_Validation_Scores.png" and "Random_Forest-Feature_Importances.png"
nd a proximity matrix for the model development in "model_proximity_matrix.csv". Feature metrics for the training data are found in the "model_premodel_table.csv" file.