# Changelog

Only major/minor changes recorded. Typos and trivial syntax errors are not included.

## 0.11.9

* Fix scipy feature removal "bug" (i.e. change attribute name)

## 0.11.8

* Completely synchronize with bioconda

## 0.11.7

* Update setup dependencies

## 0.11.6

* Update setup dependencies

## 0.11.5

* Replace soil-dbs with more generic other-dbs
* Update documentation to reflect other-dbs change
* Add additional Zenodo information to documentation

## 0.11.4

* Add in --dbs-dir to avoid specifying each-and-every database
* Update documentation
* Fix index error

## 0.11.3

* Updated model to be compatible with newer versions of scikit-learn
* Update documentation to include database downloads

## 0.11.2

* Reduce installation requirements by making Vaex optional
* Fix bad log

## 0.11.1

* Include benchmark model

## 0.11.0

* Replace native pandas dataframe for processing accession2taxid file with vaex dataframe
* THIS document

## 0.10.1 and prior

* Switch from private to public repository
* Convert text(s) to use f-strings
* Switch to pathlib from os.path wherever possible
